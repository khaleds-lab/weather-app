package com.khaled.weatherapp.service;

import com.khaled.weatherapp.dao.UserCityRepository;
import com.khaled.weatherapp.entity.City;
import com.khaled.weatherapp.entity.User;
import com.khaled.weatherapp.entity.UserCity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Khaled Ahmed
 */
@Service
public class UserCityRepositoryImplementation {

    private static final Logger logger = LoggerFactory.getLogger(UserCityRepositoryImplementation.class);

    private final UserCityRepository userCityRepository;

    private final ServiceRepository<User> userService;

    private final ServiceRepository<City> cityService;

    private final WeatherService weatherService;

    private User loggedInUser;

    @Autowired
    public UserCityRepositoryImplementation(UserCityRepository userCityRepository, @Qualifier("user") ServiceRepository<User> userService, @Qualifier("city") ServiceRepository<City> cityService, WeatherService weatherService) {
        this.userCityRepository = userCityRepository;
        this.userService = userService;
        this.cityService = cityService;
        this.weatherService = weatherService;
    }

    public String add(City theCity, Model model) {

        boolean invalidCity = false;
        boolean cityExists = false;

        if (weatherService.checkIfCityExistsFromServer(theCity).equals(HttpStatus.OK)) {

            if (!this.checkIfCityExitsForUser(theCity)) {

                if (!this.cityService.exists(theCity)) {
                    this.cityService.add(theCity);
                } else {
                    theCity = this.findCityByCityName(theCity.getCityName());
                }

                logger.info("Adding City \"{}\" for User \"{}\"", theCity.getCityName(), this.loggedInUser.getUsername());
                this.userCityRepository.save(new UserCity(this.loggedInUser, theCity));

                return "redirect:/weather/citiesList";
            } else {
                logger.info("City is already added: {}", theCity.getCityName());
                cityExists = true;
            }
        } else {
            logger.info("Invalid City name: {}", theCity.getCityName());
            invalidCity = true;
        }
        model.addAttribute("logErrorInvalid", invalidCity);
        model.addAttribute("logErrorExists", cityExists);
        return "city/add-city-form";
    }

    public String delete(int cityId) {
        if (this.cityService.existsById(cityId)) {
            City city = this.cityService.get(cityId);
            logger.info("Deleting City \"{}\" for User \"{}\"", city.getCityName(), this.loggedInUser.getUsername());

            this.userCityRepository.deleteByCityId(cityId, this.loggedInUser.getId());
        } else {
            logger.info("Deleting CityId \"{}\" for User \"{}\" FAILED", cityId, this.loggedInUser.getUsername());
        }
        return "redirect:/weather/citiesList";
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public User getLoggedInUser() {
        return this.loggedInUser;
    }

    public List<City> getUserWithCitiesWeather() {
        this.getAllCityValuesUpdated();
        return weatherService.userWithCitiesWeather;
    }

    private void getAllCityValuesUpdated() {
        weatherService.userWithCitiesWeather = new ArrayList<>();

        if (this.loggedInUser != null) {
            List<City> userCities = findAllCitiesForUser(this.loggedInUser);
            userCities.forEach(weatherService::getCurrentCityWeather);
        }
    }

    private List<City> findAllCitiesForUser(User user) {
        List<Integer> cityIdsByUserId = this.userCityRepository.findAllCityIdByUserId(user.getId());
        return this.cityService.findAllById(cityIdsByUserId);
    }

    public City findCityByCityName(String cityName) {
        return this.cityService.findByValue(cityName);
    }

    public User findUserByUsername(String username) {
        return this.userService.findByValue(username);
    }

    private boolean checkIfCityExitsForUser(City theCity) {
        List<City> cities = this.findAllCitiesForUser(loggedInUser);
        City cityToCheck = this.findCityByCityName(theCity.getCityName());
        return cities.contains(cityToCheck);
    }

}
