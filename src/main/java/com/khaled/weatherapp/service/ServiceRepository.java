package com.khaled.weatherapp.service;

import java.util.List;

/**
 * @author Khaled Ahmed
 */
public interface ServiceRepository<T> {

    /**
     * Retrieves all data from database
     *
     * @return list of values from database
     */
    List<T> findAll();

    /**
     * Retrieves all data from database by mapped to each id
     *
     * @param ids ids required to be fetched
     * @return list of values associated with given ids
     */
    List<T> findAllById(Iterable<Integer> ids);

    /**
     * Retrieves an object from database by specific value in table
     *
     * @param value needed to be identified
     * @return object from database
     */
    T findByValue(String value);

    /**
     * Checks whether id is present in database
     *
     * @param id to specify object
     */
    boolean existsById(int id);

    /**
     * Checks whether object is present in database
     *
     * @param value to be found
     */
    boolean exists(T value);

    /**
     * Fetch value from database by id
     *
     * @param id to specify object
     * @return object indicated by id
     */
    T get(int id);

    /**
     * Adds value into the database
     *
     * @param value to be added
     */
    void add(T value);

    /**
     * Deletes value from the database
     *
     * @param id of value to be deleted
     */
    void delete(int id);

    /**
     * Deletes all values in table from the database
     */
    void deleteAll();
}
