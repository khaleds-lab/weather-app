package com.khaled.weatherapp.service;

import com.khaled.weatherapp.dao.CityRepository;
import com.khaled.weatherapp.entity.City;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Khaled Ahmed
 */
@Service("city")
public class CityServiceImplementation implements ServiceRepository<City> {

    private final CityRepository cityRepository;

    public CityServiceImplementation(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public List<City> findAll() {
        return this.cityRepository.findAll();
    }

    @Override
    public List<City> findAllById(Iterable<Integer> ids) {
        return this.cityRepository.findAllById(ids);
    }

    @Override
    public City findByValue(String value) {
        return this.cityRepository.findByCityName(value);
    }

    @Override
    public boolean existsById(int id) {
        return this.cityRepository.existsById(id);
    }

    @Override
    public boolean exists(City city) {
        if (city != null) {
            return this.cityRepository.existsByCityName(city.getCityName());
        }
        return false;
    }

    @Override
    public City get(int id) {
        Optional<City> result = this.cityRepository.findById(id);
        City city;
        if (result.isPresent()) {
            city = result.get();
        } else {
            throw new IllegalArgumentException("City not found: " + id);
        }
        return city;
    }

    @Override
    public void add(City city) {
        if (city != null) {
            this.cityRepository.save(city);
        }
    }


    public void addAll(Iterable<City> cities) {
        if (cities != null) {
            cityRepository.saveAll(cities);
        }
    }

    @Override
    public void delete(int id) {
        if (this.cityRepository.existsById(id)) {
            this.cityRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("City does not exist: " + id);
        }
    }

    @Override
    public void deleteAll() {
        this.cityRepository.deleteAll();
    }

}
