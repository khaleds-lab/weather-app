package com.khaled.weatherapp.service;

import com.khaled.weatherapp.entity.City;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;
import java.util.List;

/**
 * @author Khaled Ahmed
 */
@Service
public class WeatherService {

    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    private final RestTemplate restTemplate;

    public List<City> userWithCitiesWeather;

    private final String WEATHER_URL =
            "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}&units={metric}";

    @Value("${api.key}")
    private String apiKey;

    public WeatherService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void getCurrentCityWeather(City theCity) throws JSONException {
        logger.info("Requesting current weather for {}", theCity.getCityName());
        this.updateCurrentCity(theCity);
    }

    private void updateCurrentCity(City theCity) {
        ResponseEntity<String> response = getWeatherData(theCity);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            JSONObject jsonObject = new JSONObject(response.getBody());
            this.updateCitiesTempAndHumid(jsonObject, theCity);
        }
    }

    private void updateCitiesTempAndHumid(JSONObject jsonObject, City theCity) {
        theCity.setTemp(jsonObject.getJSONObject("main").get("temp").toString() + "°C");
        theCity.setHumidity(jsonObject.getJSONObject("main").get("humidity").toString() + "%");
        this.userWithCitiesWeather.add(theCity);
    }

    public HttpStatus checkIfCityExistsFromServer(City theCity) {
        return this.getWeatherData(theCity).getStatusCode();
    }

    private ResponseEntity<String> getWeatherData(City theCity) {
        RequestEntity<?> request;
        URI url = new UriTemplate(this.WEATHER_URL).expand(theCity.getCityName(), this.apiKey, "metric");
        request = RequestEntity.get(url).accept(MediaType.APPLICATION_JSON).build();

        return this.exchangeRequestToResponse(request, theCity);
    }

    private ResponseEntity<String> exchangeRequestToResponse(RequestEntity<?> request, City theCity) {
        ResponseEntity<String> response;
        try {
            response = this.restTemplate.exchange(request, String.class);
        } catch (HttpClientErrorException e) {
            logger.info("City not found: {}", theCity.getCityName());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return response;
    }

}
