package com.khaled.weatherapp.service;

import com.khaled.weatherapp.dao.UserRepository;
import com.khaled.weatherapp.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Khaled Ahmed
 */
@Service("user")
public class UserServiceImplementation implements ServiceRepository<User> {

    private final UserRepository userRepository;

    public UserServiceImplementation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    @Override
    public List<User> findAllById(Iterable<Integer> ids) {
        return this.userRepository.findAllById(ids);
    }

    @Override
    public User findByValue(String username) {
        return this.userRepository.findByUsername(username);
    }

    @Override
    public boolean existsById(int id) {
        return this.userRepository.existsById(id);
    }

    @Override
    public boolean exists(User user) {
        if (user != null) {
            return this.userRepository.existsUserByUsername(user.getUsername());
        }
        return false;
    }

    @Override
    public User get(int id) {
        Optional<User> result = this.userRepository.findById(id);
        User user;

        if (result.isPresent()) {
            user = result.get();
        } else {
            throw new RuntimeException("User not found: " + id);
        }
        return user;
    }

    @Override
    public void add(User user) {
        if (user != null) {
            this.userRepository.save(user);
        }
    }

    @Override
    public void delete(int id) {
        if (this.userRepository.existsById(id)) {
            this.userRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("User does not exist: " + id);
        }
    }

    @Override
    public void deleteAll() {
        this.userRepository.deleteAll();
    }

}
