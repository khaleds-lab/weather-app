package com.khaled.weatherapp.dao;

import com.khaled.weatherapp.entity.UserCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserCityRepository extends JpaRepository<UserCity, Integer> {

    @Query(value = "SELECT city_id FROM user_city where user_id=?1", nativeQuery = true)
    List<Integer> findAllCityIdByUserId(int id);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM user_city where city_id=?1 AND user_id=?2", nativeQuery = true)
    void deleteByCityId(int userId, int cityId);
}
