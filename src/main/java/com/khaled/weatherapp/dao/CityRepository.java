package com.khaled.weatherapp.dao;

import com.khaled.weatherapp.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {

    City findByCityName(String cityName);

    boolean existsByCityName(String cityName);
}
