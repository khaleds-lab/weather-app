package com.khaled.weatherapp.dao;

import com.khaled.weatherapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    boolean existsUserByUsername(String username);
}
