package com.khaled.weatherapp.rest;

import com.khaled.weatherapp.entity.City;
import com.khaled.weatherapp.entity.User;

import com.khaled.weatherapp.service.UserCityRepositoryImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Weather App that allows User to sign in and
 * get current Temperature and Humidity for any given City
 *
 * @author Khaled Ahmed
 */
@Controller
public class WeatherController {

    private static final Logger logger = LoggerFactory.getLogger(WeatherController.class);

    private final UserCityRepositoryImplementation userCityService;

    @Autowired
    public WeatherController(UserCityRepositoryImplementation userCityService) {
        this.userCityService = userCityService;
    }

    /**
     * Default Login page
     *
     * @return the login page
     */
    @RequestMapping("/")
    public String showLoginForm() {
        return "redirect:/login";
    }

    /**
     * Processes the logged in User and injects it into the loggedInUser
     *
     * @return list of the current cities for logged in User
     */
    @RequestMapping("/weather/processForm")
    public String processLoginForm() {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        User loggedInUser = this.userCityService.findUserByUsername(username);

        this.userCityService.setLoggedInUser(loggedInUser);
        logger.info("Logged In User: {}", loggedInUser.getUsername());

        return "redirect:/weather/citiesList";
    }

    /**
     * Fetches all cities mapped to logged in User
     *
     * @param model displays objects from classes into HTML files
     * @return list of the current cities for logged in User
     */
    @GetMapping("/weather/citiesList")
    public String getCities(Model model) {
        User loggedInUser = this.userCityService.getLoggedInUser();
        if (loggedInUser != null) {
            model.addAttribute("username", loggedInUser.getUsername());
            model.addAttribute("cities", this.userCityService.getUserWithCitiesWeather());
            return "city/list-cities";
        } else {
            return "redirect:/login";
        }
    }

    /**
     * A form used to add cities for a User.
     *
     * @param model displays objects from classes into HTML files
     * @return the city added to the add method
     */
    @GetMapping("/weather/showFormForAdd")
    public String showFormForAdd(Model model) {
        City theCity = new City();
        model.addAttribute("city", theCity);
        return "city/add-city-form";
    }

    /**
     * Adds a link between logged in User and added City.
     *
     * @param theCity value of the city added by user
     * @param model   displays objects from classes into HTML files
     * @return list of the current cities for logged in User
     */
    @PostMapping("/weather/add")
    public String addCity(@ModelAttribute("city") City theCity, Model model) {
        return this.userCityService.add(theCity, model);
    }

    /**
     * Deletes the link between logged in User and the City.
     *
     * @param cityId id of city to be removed from user.
     * @return list of the current cities for logged in User
     */
    @GetMapping("/weather/delete")
    public String deleteCity(@RequestParam("cityId") int cityId) {
        return this.userCityService.delete(cityId);
    }

}
