package com.khaled.weatherapp.entity;

import javax.persistence.*;

/**
 * @author Khaled Ahmed
 */
@Entity
@Table(name = "user_city")
public class UserCity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    private City city;

    public UserCity(User user, City city) {
        this.user = user;
        this.city = city;
    }

    public UserCity() {
    }

    public User getUser() {
        return this.user;
    }

    public City getCity() {
        return this.city;
    }

}
