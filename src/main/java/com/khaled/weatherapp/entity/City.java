package com.khaled.weatherapp.entity;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Khaled Ahmed
 */
@Entity
@Table(name = "city")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "city_name")
    private String cityName;

    @Transient
    private String temp;

    @Transient
    private String humidity;

    public City() {
    }

    public City(String cityName) {
        this.cityName = cityName;
    }

    public int getId() {
        return this.id;
    }

    public String getCityName() {
        return this.cityName;
    }

    public String getTemp() {
        return this.temp;
    }

    public String getHumidity() {
        return this.humidity;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return id == city.id &&
                cityName.equals(city.cityName) &&
                Objects.equals(temp, city.temp) &&
                Objects.equals(humidity, city.humidity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cityName, temp, humidity);
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + this.id +
                ", cityName='" + this.cityName + '\'' +
                ", temp='" + this.temp + '\'' +
                ", humidity='" + this.humidity + '\'' +
                '}';
    }

}
