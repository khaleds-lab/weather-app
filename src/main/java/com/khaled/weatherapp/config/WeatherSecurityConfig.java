package com.khaled.weatherapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

/**
 * @author Khaled Ahmed
 */
@Configuration
public class WeatherSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource securityDataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.jdbcAuthentication()
                .dataSource(securityDataSource)
                .usersByUsernameQuery("SELECT username, password,'true' as enabled FROM user where username = ?")
                .authoritiesByUsernameQuery("SELECT username, 'ROLE_USER' FROM user  WHERE username=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/weather/**").hasRole("USER")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .successForwardUrl("/weather/processForm")
                .permitAll()
                .and()
                .logout()
                .permitAll();
    }

}
