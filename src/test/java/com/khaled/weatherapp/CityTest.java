package com.khaled.weatherapp;

import com.khaled.weatherapp.entity.City;
import com.khaled.weatherapp.service.CityServiceImplementation;
import com.khaled.weatherapp.service.UserCityRepositoryImplementation;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.ArrayList;
import java.util.List;

//@Testcontainers
@SpringBootTest
@Sql(scripts = {"classpath:test-weather.sql"})
@ActiveProfiles("test")
public class CityTest extends MySQLContainerTest {

    @Autowired
    UserCityRepositoryImplementation userCity;

    @Autowired
    CityServiceImplementation city;

    @Test
    void addCity() {
        //prepare
        City addCity = new City("Essen");

        //test
        city.add(addCity);
        City getCity = userCity.findCityByCityName("Essen");

        //assert
        assertThat(addCity).isEqualTo(getCity);
    }

    @Test
    void testDeleteCity() {
        //prepare
        City cityToAdd = new City("Cologne");
        city.add(cityToAdd);

        //test
        city.delete(cityToAdd.getId());

        //assert
        assertThat(userCity.findCityByCityName("Cologne")).isNull();
    }

    @Test
    void testGetCityById() {
        //prepare
        City cityToAdd = new City("Munich");
        city.add(cityToAdd);

        //test
        City getCityById = city.get(cityToAdd.getId());

        //assert
        assertThat(getCityById).isEqualTo(cityToAdd);
    }

    @Test
    void testFindAll() {
        //prepare
        City cityToAdd1 = new City("Berlin");
        City cityToAdd2 = new City("Frankfurt");
        City cityToAdd3 = new City("Hamburg");

        List<City> cities = new ArrayList<>();
        cities.add(cityToAdd1);
        cities.add(cityToAdd2);
        cities.add(cityToAdd3);
        city.addAll(cities);

        //test
        List<City> getAllCities = city.findAll();

        //assert
        assertThat(getAllCities).isEqualTo(cities);
    }

}
