# Weather app

**Getting started**

To use this app, you need to register an http://openweathermap.org/appid[API Key] on the
http://openweathermap.org/[OpenWeatherApp] service.

Find `application.properties` in `src/main/resources` and add your
API key there:

```
api.key=<yourkey>
```
== to use with Database,
start your database server locally and add the JDBC properties into the `application.properties`:

```
spring.datasource.url=
spring.datasource.username=
spring.datasourse.password=
```
You can find a Schema in `src/test/resources/test-weather.sql` to use when creating your Database.